package httpapi

import akka.http.javadsl.model.headers.HttpCredentials.createOAuth2BearerToken
import akka.http.scaladsl.model.StatusCodes.Unauthorized
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import api.httpapi.GainyHttpApi
import api.httpapi.requestmodels.{Job, LogPass}
import com.github.t3hnar.bcrypt._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import models.{GainyJob, GainyJobState, GainyUser}
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.flatspec.AsyncFlatSpec
import service.GainyService
import io.circe.generic.auto._
import io.circe.syntax._
import service.servicemodels.{AuthData, Token}

import scala.concurrent.Future

class GainyHttpApiTest extends AsyncFlatSpec with AsyncMockFactory with ScalatestRouteTest {
  "user/register" should "return user" in {
    val serviceMock = mock[GainyService]

    val log = "login"
    val pass = "pass"

    val user = GainyUser(1, log, pass.bcrypt)

    (serviceMock.register _)
      .expects(log, pass)
      .returning(
        Future.successful(user)
      ).once()

    val httpApi = new GainyHttpApi(serviceMock)

    val entity = HttpEntity(
      ContentTypes.`application/json`,
      LogPass(log, pass).asJson.toString
    )

    Post("/user/register", entity) ~> httpApi.routes ~> check {
      assert(responseAs[GainyUser] == user)
    }
  }

  "user/gettoken" should "return token" in {
    val serviceMock = mock[GainyService]

    val log = "login"
    val pass = "pass"

    val token = Token("token")

    (serviceMock.getToken _)
      .expects(log, pass)
      .returning(
        Future.successful(token)
      ).once()

    val httpApi = new GainyHttpApi(serviceMock)

    val entity = HttpEntity(
      ContentTypes.`application/json`,
      LogPass(log, pass).asJson.toString
    )

    Get("/user/gettoken", entity) ~> httpApi.routes ~> check {
      assert(responseAs[Token] == token)
    }
  }

  "api" should "forbid invalid token" in {
    val serviceMock = mock[GainyService]

    val token = "token"
    val job = Job("api-token", "figi", 0, 0, 0, 0)

    (serviceMock.validateToken _)
      .expects(token)
      .returning(
        Future.successful(None)
      ).twice()

    val httpApi = new GainyHttpApi(serviceMock)

    val entity = HttpEntity(
      ContentTypes.`application/json`,
      job.asJson.toString
    )

    val reqCreate = Post("/job/create", entity)
      .addCredentials(createOAuth2BearerToken(token))

    val reqList = Get("/job/list", entity)
      .addCredentials(createOAuth2BearerToken(token))

    reqCreate ~> httpApi.routes ~> check {
      assert(status == Unauthorized)
    }

    reqList ~> httpApi.routes ~> check {
      assert(status == Unauthorized)
    }
  }

  "job/create" should "create job with valid token" in {
    val serviceMock = mock[GainyService]

    val token = "token"
    val auth = AuthData("login")

    val apiToken = "token"
    val figi = "figi"
    val lots: Long = 10
    val refPrice: BigDecimal = 55.1
    val profit: BigDecimal = 0.01
    val loss: BigDecimal = 0.02

    val reqJob = Job(apiToken, figi, lots, refPrice, profit, loss)
    val job = GainyJob(1, 1, apiToken, figi, lots, refPrice, profit, loss, GainyJobState.Running)

    (serviceMock.validateToken _)
      .expects(token)
      .returning(
        Future.successful(Some(auth))
      ).once()

    (serviceMock.createJob(_: AuthData)(
      _: String, _: String,
      _: Long, _: BigDecimal,
      _: BigDecimal, _: BigDecimal
    )).expects(auth, apiToken, figi, lots, refPrice, profit, loss)
      .returning(Future.successful(job))

    val httpApi = new GainyHttpApi(serviceMock)

    val entity = HttpEntity(
      ContentTypes.`application/json`,
      reqJob.asJson.toString
    )

    val req = Post("/job/create", entity)
      .addCredentials(createOAuth2BearerToken(token))

    req ~> httpApi.routes ~> check {
      assert(responseAs[GainyJob] == job)
    }

  }

  "job/list" should "list jobs with valid token" in {
    val serviceMock = mock[GainyService]

    val token = "token"
    val auth = AuthData("login")

    val jobs = Seq(
      GainyJob(1, 1, "token1", "figi1", 10, 50.0, 0.1, 0.02, GainyJobState.Running),
      GainyJob(2, 1, "token2", "figi2", 100, 100.0, 0.01, 0.2, GainyJobState.Failed),
      GainyJob(3, 1, "token3", "figi3", 20, 30.0, 0.3, 0.05, GainyJobState.Running)
    )

    (serviceMock.validateToken _)
      .expects(token)
      .returning(
        Future.successful(Some(auth))
      ).once()

    (serviceMock.listJobs(_: AuthData)())
      .expects(auth)
      .returning(Future.successful(jobs))
      .once()

    val httpApi = new GainyHttpApi(serviceMock)

    val req = Get("/job/list")
      .addCredentials(createOAuth2BearerToken(token))

    req ~> httpApi.routes ~> check {
      assert(responseAs[Seq[GainyJob]] == jobs)
    }
  }
}
