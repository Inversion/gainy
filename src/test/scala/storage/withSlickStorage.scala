package storage

import java.util.UUID

import com.github.t3hnar.bcrypt._
import models.{GainyJob, GainyJobState, GainyUser}
import org.scalatest.{AsyncTestSuite, compatible}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import storage.slickstorage.SlickGainyStorage
import storage.slickstorage.jobs.GainyJobsQueryRepository.GainyJobs
import storage.slickstorage.users.GainyUsersQueryRepository.GainyUsers

import scala.concurrent.Future

trait withSlickStorage { this : AsyncTestSuite =>
  def withStorage(test: GainyStorage => Future[compatible.Assertion]) : Future[compatible.Assertion] = {
    val db = Database.forURL(
      s"jdbc:h2:mem:${UUID.randomUUID()}",
      driver = "org.h2.Driver",
      keepAliveConnection = true
    )

    val storage = new SlickGainyStorage(db)

    db.run(
      initSchema
        .andThen(GainyUsers ++= sampleUsers)
        .andThen(GainyJobs ++= sampleJobs)
    ).flatMap(_ => test(storage)).andThen { case _ => db.close() }
  }

  val initSchema = (GainyUsers.schema ++ GainyJobs.schema).create

  // ids should autoincrement
  val sampleUsers = Seq(
    GainyUser(1, "Anna", "qwerty".bcrypt),
    GainyUser(2, "Jack", "123".bcrypt),
    GainyUser(3, "Sam", "321".bcrypt),
  )

  // ids should autoincrement
  val sampleJobs = Seq(
    GainyJob(1, 1, "token1", "figi1", 10, 50.0, 0.1, 0.02, GainyJobState.Running),
    GainyJob(2, 1, "token2", "figi2", 100, 100.0, 0.01, 0.2, GainyJobState.Failed),
    GainyJob(3, 3, "token3", "figi3", 20, 30.0, 0.3, 0.05, GainyJobState.Running),
    GainyJob(4, 3, "token4", "figi4", 110, 52.0, 0.11, 0.72, GainyJobState.Running),
    GainyJob(5, 2, "token5", "figi5", 1, 42.0, 0.05, 0.22, GainyJobState.Succeed),
    GainyJob(6, 3, "token6", "figi6", 8, 30.6, 0.32, 0.08, GainyJobState.Succeed),
    GainyJob(7, 2, "token7", "figi7", 42, 51.1, 0.45, 0.01, GainyJobState.Failed),
    GainyJob(8, 1, "token10", "figi8", 50, 4.5, 0.51, 0.02, GainyJobState.Running)
  )
}
