package storage

import com.github.t3hnar.bcrypt._
import models.{GainyJobState, GainyUser}
import org.scalatest.Succeeded
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import service.exceptions.LoginAlreadyTakenException

import scala.concurrent.Future

class GainySlickStorageTest extends AsyncFlatSpec
  with withSlickStorage
  with Matchers {

  "insertUser" should "insert user" in withStorage {
    storage => {
      val user = GainyUser(0, "Tester", "haha".bcrypt)
      for {
        inserted <- storage.insertUser(user.login, user.hash)
      } yield assert(inserted == user.copy(id=inserted.id))
    }
  }

  "insertUser" should "fail with existing login" in withStorage {
    storage => {
      val user = sampleUsers.head
      recoverToSucceededIf[LoginAlreadyTakenException] {
        storage.insertUser(user.login, user.hash)
      }
    }
  }

  "findUser" should "find existing user" in withStorage {
    storage => {
      Future.traverse(sampleUsers) { user =>
        storage.findUser(user.login).map(
          _.contains(user)
        ).map(assert(_))
      }.map(_.forall(_ == Succeeded)).map(assert(_))
    }
  }

  "findUser" should "not find non existing user" in withStorage {
    storage => {
      for {
        found <- storage.findUser("anon")
      } yield assert(found.isEmpty)
    }
  }

  "findUser" should "find inserted user" in withStorage {
    storage => {
      val login = "Tester"
      for {
        inserted <- storage.insertUser(login, "haha".bcrypt)
        found <- storage.findUser(login)
      } yield assert(found.contains(inserted))
    }
  }

  "listJobs" should "list user jobs" in withStorage {
    storage => {
      Future.traverse(sampleUsers) ( user =>
        storage.listJobs(user.login).map ( jobs => {
          // TODO Fix strange bugs if BigDecimal fields are not round
          /*
          println(user)
          sampleJobs.filter(_.userId == user.id).foreach(println)
          println("----")
          jobs.foreach(println)
          println("++++")
          */
          jobs should contain theSameElementsAs sampleJobs.filter(_.userId == user.id)
        })
      ).map(_.forall(_ == Succeeded)).map(assert(_))
    }
  }

  "listRunningJobs" should "list running jobs" in withStorage {
    storage => {
      storage.listRunningJobs().map ( jobs => {
        jobs should contain theSameElementsAs sampleJobs.filter(
          _.state == GainyJobState.Running
        )
      })
    }
  }
}
