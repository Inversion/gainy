package service

import com.github.nscala_time.time.Imports.DateTime
import com.github.t3hnar.bcrypt._
import investapi.InvestApi
import investapi.investmodels.{Candle, Candles, MarketOperation, MarketOrderResponse}
import investapi.investmodels.CandlesInterval.CandlesInterval
import investapi.investmodels.MarketOperation.MarketOperation
import models.{GainyJob, GainyJobState, GainyUser}
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.flatspec.AsyncFlatSpec
import service.exceptions.{InvalidCredentialsException, UserNotFoundException}
import service.serviceconfig.GainyConfig
import service.servicemodels.AuthData
import storage.GainyStorage

import scala.concurrent.Future

class GainyServiceTest extends AsyncFlatSpec with AsyncMockFactory {
  val config = GainyConfig("secret_key", 10)

  "register" should "register user" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val log = "login"
    val pass = "password"

    (storageMock.insertUser _)
      .expects(log, *)
      .onCall(
        (log, hash) => Future.successful(
          GainyUser(1, log, hash)
        )
      ).once()

    val service = new GainyService(config, investApiMock, storageMock)

    service.register(log, pass).map( user => {
      assert(user.login == log && pass.isBcrypted(user.hash))
    })
  }

  "getToken" should "generate token for existing user" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val log = "login"
    val pass = "password"

    (storageMock.findUser _)
      .expects(log)
      .returning(Future.successful(
          Some(GainyUser(1, log, pass.bcrypt))
      )).once()

    val service = new GainyService(config, investApiMock, storageMock)

    service.getToken(log, pass).map(_ => assert(true))
  }

  "getToken" should "fail for non existing user" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val log = "login"
    val pass = "password"

    (storageMock.findUser _)
      .expects(log)
      .returning(Future.successful(None)).once()

    val service = new GainyService(config, investApiMock, storageMock)

    recoverToSucceededIf[InvalidCredentialsException] {
      service.getToken(log, pass)
    }
  }

  "createJob" should "create job for existing user" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val id = 1
    val log = "login"
    val hash = "hash"

    val apiToken = "token"
    val figi = "figi"
    val lots: Long = 10
    val refPrice: BigDecimal = 55.1
    val profit: BigDecimal = 0.01
    val loss: BigDecimal = 0.02

    val job = GainyJob(1, id, apiToken, figi, lots,
      refPrice, profit, loss, GainyJobState.Running)

    (storageMock.findUser _)
      .expects(log)
      .returning(Future.successful(
        Some(GainyUser(id, log, hash))
      )).once()

    (storageMock.insertJob _)
      .expects(id, apiToken, figi, lots, refPrice, profit, loss)
      .returning(
        Future.successful(job)
      ).once()

    val service = new GainyService(config, investApiMock, storageMock)

    service.createJob(AuthData(log))(
      apiToken, figi, lots, refPrice, profit, loss
    ).map (res => assert(res == job))
  }

  "createJob" should "fail for non existing user" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val log = "login"

    val apiToken = "token"
    val figi = "figi"
    val lots: Long = 10
    val refPrice: BigDecimal = 55.1
    val profit: BigDecimal = 0.01
    val loss: BigDecimal = 0.02

    (storageMock.findUser _)
      .expects(log)
      .returning(Future.successful(None))
      .once()

    val service = new GainyService(config, investApiMock, storageMock)

    recoverToSucceededIf[UserNotFoundException] {
      service.createJob(AuthData(log))(
        apiToken, figi, lots, refPrice, profit, loss
      )
    }
  }

  "validateToken" should "return None for invalid token" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val service = new GainyService(config, investApiMock, storageMock)

    service.validateToken("nonsense").map(
      res => assert(res.isEmpty)
    )
  }

  "validateToken" should "validate generated token" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val log = "login"
    val pass = "password"

    (storageMock.findUser _)
      .expects(log)
      .returning(Future.successful(
        Some(GainyUser(1, log, pass.bcrypt))
      )).once()

    val service = new GainyService(config, investApiMock, storageMock)

    service.getToken(log, pass).flatMap(res =>
      service.validateToken(res.token)
    ).map(res => assert(res.contains(AuthData(log))))
  }

  "triggerJobs" should "trigger right jobs" in {
    val storageMock = mock[GainyStorage]
    val investApiMock = mock[InvestApi]

    val risingJobs = Seq(
      GainyJob(1, 1, "token1", "figi1", 10, 50.0, 0.1, 0.02, GainyJobState.Running),
      GainyJob(2, 1, "token2", "figi2", 100, 100.0, 0.01, 0.2, GainyJobState.Running)
    )

    val fallingJobs = Seq(
      GainyJob(1, 1, "token1", "figi1", 10, 50.0, 0.1, 0.02, GainyJobState.Running),
      GainyJob(2, 1, "token2", "figi2", 100, 100.0, 0.01, 0.2, GainyJobState.Running)
    )

    val coldJobs = Seq(
      GainyJob(5, 2, "token5", "figi5", 1, 42.0, 0.05, 0.22, GainyJobState.Running),
      GainyJob(6, 3, "token6", "figi6", 8, 30.6, 0.32, 0.08, GainyJobState.Running),
      GainyJob(7, 2, "token7", "figi7", 42, 51.1, 0.45, 0.01, GainyJobState.Running),
      GainyJob(8, 1, "token10", "figi8", 50, 4.5, 0.51, 0.02, GainyJobState.Running)
    )

    val jobs = risingJobs ++ fallingJobs ++ coldJobs

    def find(token: String, figi: String, jobs: Seq[GainyJob]) : Option[GainyJob] =
      jobs.find(job => job.apiToken == token && job.figi == figi)

    def isIn(token: String, figi: String, jobs: Seq[GainyJob]) : Boolean =
      find(token, figi, jobs).isDefined

    (storageMock.listRunningJobs _)
      .expects()
      .returning(Future.successful(jobs))
      .once()

    (investApiMock.marketCandles(_: String)(
      _: String, _: DateTime, _: DateTime, _: CandlesInterval
    )).expects(where {
      (token, figi, _, _, _) => isIn(token, figi, jobs)
    }).onCall(
      (token, figi, _, _, _) =>
        if (isIn(token, figi, risingJobs)) {
          val job = find(token, figi, risingJobs).get
          val price = job.refPrice * (1.1 + job.profit)
          Future.successful(
            Candles(Seq(Candle(price)), figi)
          )
        } else if (isIn(token, figi, fallingJobs)) {
          val job = find(token, figi, fallingJobs).get
          val price = job.refPrice * (0.9 - job.loss)
          Future.successful(
            Candles(Seq(Candle(price)), figi)
          )
        } else {
          val job = find(token, figi, coldJobs).get
          Future.successful(
            Candles(Seq(Candle(job.refPrice)), figi)
          )
        }
    ).repeated(jobs.size)

    (investApiMock.ordersMarketOrder(_: String)(
      _: String, _: Long, _: MarketOperation
    ))
      .expects(where {
        (token, figi, _, op) =>
          isIn(token, figi, risingJobs ++ fallingJobs) &&
            op == MarketOperation.Sell
    })
      .returning(Future.successful(MarketOrderResponse()))
      .repeated(risingJobs.size + fallingJobs.size)

    val service = new GainyService(config, investApiMock, storageMock)

    service.triggerJobs().map(_ => assert(true))
  }
}
