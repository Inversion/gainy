package api.httpapi.requestmodels

case class Job(
                apiToken: String,
                figi: String,
                lots: Long,
                refPrice: BigDecimal,
                profit: BigDecimal,
                loss: BigDecimal
              )
