package api.httpapi.requestmodels

case class LogPass(
                    login: String,
                    password: String
                  )
