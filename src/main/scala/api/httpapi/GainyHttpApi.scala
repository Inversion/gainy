package api.httpapi

import akka.http.scaladsl.model.StatusCodes.{BadRequest, Forbidden, Unauthorized}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.Credentials
import akka.http.scaladsl.server.{AuthenticationFailedRejection, ExceptionHandler, MalformedRequestContentRejection, RejectionHandler, RequestEntityExpectedRejection, Route}
import api.httpapi.requestmodels.{Job, LogPass}
import api.httpapi.responsemodels.ErrorResponse
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import service.GainyService
import service.exceptions.{InvalidCredentialsException, LoginAlreadyTakenException, TokenExpiredException, UserNotFoundException}

import scala.concurrent.Future

class GainyHttpApi(service: GainyService) {

  implicit def exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case e: InvalidCredentialsException =>
        complete(Forbidden, ErrorResponse(e.getMessage))
      case e: LoginAlreadyTakenException =>
        complete(Forbidden, ErrorResponse(e.getMessage))
      case e: TokenExpiredException =>
        complete(Unauthorized, ErrorResponse(e.getMessage))
      case e: UserNotFoundException =>
        complete(Unauthorized, ErrorResponse(e.getMessage))
    }

  implicit def rejectionHandler = RejectionHandler.newBuilder()
    .handle {
      case AuthenticationFailedRejection(_, _) =>
        complete(Unauthorized, ErrorResponse("Failed to authorize"))
    }
    .handle {
      case RequestEntityExpectedRejection =>
        complete(BadRequest, ErrorResponse("Request has no entity"))
    }
    .handle {
      case MalformedRequestContentRejection(_, _) =>
        complete(BadRequest, ErrorResponse("Request is malformed"))
    }
    .result()

  val bearerAuth = (creds: Credentials) =>
    creds match {
      case Credentials.Provided(token) =>
        service.validateToken(token)
      case _ => Future.successful(None)
    }

  val userRoutes: Route = pathPrefix("user") {
    (post & path("register")) {
      entity(as[LogPass]) {
        data => complete(service.register(data.login, data.password))
      }
    } ~ (get & path("gettoken")) {
      entity(as[LogPass]) {
        data => complete(service.getToken(data.login, data.password))
      }
    }
  }

  val jobRoutes: Route = pathPrefix("job") {
    (post & path("create")) {
      authenticateOAuth2Async("create-job", bearerAuth) {
        data => {
          entity(as[Job]) {
            job =>
              complete(service.createJob(data)(
                job.apiToken, job.figi, job.lots, job.refPrice, job.profit, job.loss
              ))
          }
        }
      }
    } ~ (get & path("list")) {
      authenticateOAuth2Async("list-jobs", bearerAuth) {
        data => {
          complete(service.listJobs(data)())
        }
      }
    }
  }

  val routes = Route.seal(
    userRoutes ~ jobRoutes
  ) // If place it above, get NullPointerException
}
