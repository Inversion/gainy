package utils

import scala.concurrent.{ExecutionContext, Future}

object FutureUtils {
  def batchTraverse[T, U](in: Seq[T], size: Int)(f: T => Future[U])
                         (implicit ec: ExecutionContext): Future[Seq[U]] =
    in.grouped(size).foldLeft(Future.successful(Seq[U]()))(
      (acc, cur) => acc.flatMap(
        prev => Future.traverse(cur)(f).map(prev ++ _)
      )
    )
}
