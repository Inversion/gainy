package service.serviceconfig

case class GainyConfig(
                        secretKey: String,
                        jobsBatch: Int
                      )
