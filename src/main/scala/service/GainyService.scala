package service

import java.time.Instant

import com.github.nscala_time.time.Imports.DateTime

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode

import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import com.github.t3hnar.bcrypt._

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException

import investapi.InvestApi
import investapi.investmodels.{Candle, CandlesInterval, MarketOperation}

import storage.GainyStorage

import models.{GainyJob, GainyUser}

import service.exceptions._
import service.serviceconfig.GainyConfig
import service.servicemodels.{AuthData, Token}

import utils.FutureUtils

class GainyService(config: GainyConfig,
                   investApi: InvestApi,
                   storage: GainyStorage)
                  (implicit ec: ExecutionContext) {
  private val algo = JwtAlgorithm.HS256

  def register(login: String, password: String): Future[GainyUser] =
    storage.insertUser(login, password.bcrypt)

  def getToken(login: String, password: String): Future[Token] =
    storage.findUser(login).flatMap(_ match {
      case Some(user) if password.isBcrypted(user.hash) => genToken(AuthData(login))
      case _ => Future.failed(
        new InvalidCredentialsException("No user with such login or incorrect password")
      )
    })

  private def genToken(authData: AuthData): Future[Token] = {
    val claim = JwtClaim(
      content = authData.asJson.toString(),
      expiration = Some(
        Instant.now.plusSeconds(157784760).getEpochSecond
      ),
      issuedAt = Some(Instant.now.getEpochSecond)
    )

    val tokenStr = JwtCirce.encode(
      claim,
      config.secretKey,
      algo
    )

    Future.successful(Token(tokenStr))
  }

  def createJob(auth: AuthData)(apiToken: String, figi: String,
                                lots: Long, refPrice: BigDecimal,
                                profit: BigDecimal, loss: BigDecimal): Future[GainyJob] =
    storage.findUser(auth.login).flatMap(_ match {
      case Some(user) => storage.insertJob(
        user.id, apiToken, figi, lots, refPrice, profit, loss
      )
      case None => Future.failed(
        new UserNotFoundException("No user with such login")
      )
    })

  def listJobs(auth: AuthData)(): Future[Seq[GainyJob]] =
    storage.listJobs(auth.login)

  def triggerJobs(): Future[Unit] =
    storage.listRunningJobs().map(
      FutureUtils.batchTraverse(_, config.jobsBatch)(triggerJob)
    )

  def triggerJob(job: GainyJob): Future[Unit] = {
    val now = DateTime.now()

    // TODO Implement flexible interval
    investApi.marketCandles(job.apiToken)(
      job.figi, now.minusMinutes(5), now, CandlesInterval._5min
    ).map(_.candles.lastOption match {
      case Some(Candle(close)) if (
        close > job.refPrice * (1 + job.profit) ||
          close < job.refPrice * (1 - job.loss)
        ) =>
        investApi.ordersMarketOrder(job.apiToken)(
          job.figi, job.lots, MarketOperation.Sell
        ).map(_ => ()) // TODO Result handling
      case _ => Future.successful(())
    })
  }

  def validateToken(token: String): Future[Option[AuthData]] =
    JwtCirce.decode(token, config.secretKey, Seq(algo)) match {
      case Success(claim: JwtClaim) =>
        claim.expiration match {
          case Some(time) if time > Instant.now.getEpochSecond =>
            decode[AuthData](claim.content) match {
              case Right(data) => Future.successful(Some(data))
              case Left(_) => Future.successful(None)
            }
          case _ => Future.failed(
            new TokenExpiredException("Token expired")
          )
        }
      case Failure(_) => Future.successful(None)
    }
}
