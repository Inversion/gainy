package service.exceptions

class LoginAlreadyTakenException(msg: String) extends Exception(msg) {
}
