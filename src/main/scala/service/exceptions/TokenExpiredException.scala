package service.exceptions

class TokenExpiredException(msg: String) extends Exception(msg) {
}
