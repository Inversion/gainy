package service.exceptions

class InvalidCredentialsException(msg: String) extends Exception(msg) {
}
