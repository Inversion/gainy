package service.exceptions

class UserNotFoundException(msg: String) extends Exception(msg) {
}
