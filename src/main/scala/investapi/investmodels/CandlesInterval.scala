package investapi.investmodels

import investapi.investmodels
import io.circe.{Decoder, Encoder}

object CandlesInterval extends Enumeration {
  type CandlesInterval = Value
  val _1min: investmodels.CandlesInterval.Value = Value("1min")
  val _2min: investmodels.CandlesInterval.Value = Value("2min")
  val _3min: investmodels.CandlesInterval.Value = Value("3min")
  val _5min: investmodels.CandlesInterval.Value = Value("5min")
  val _10min: investmodels.CandlesInterval.Value = Value("10min")
  val _15min: investmodels.CandlesInterval.Value = Value("15min")
  val _30min: investmodels.CandlesInterval.Value = Value("30min")

  implicit val decoder: Decoder[CandlesInterval] = Decoder.decodeEnumeration(CandlesInterval)
  implicit val encoder: Encoder[CandlesInterval] = Encoder.encodeEnumeration(CandlesInterval)
}
