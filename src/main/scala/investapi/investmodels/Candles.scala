package investapi.investmodels

case class Candles(
                    candles: Seq[Candle],
                    figi: String
                  )
