package investapi.investmodels

import investapi.investmodels
import io.circe.{Decoder, Encoder}

object MarketOperation extends Enumeration {
  type MarketOperation = Value
  val Buy: investmodels.MarketOperation.Value = Value("Buy")
  val Sell: investmodels.MarketOperation.Value = Value("Sell")

  implicit val decoder: Decoder[MarketOperation] = Decoder.decodeEnumeration(MarketOperation)
  implicit val encoder: Encoder[MarketOperation] = Encoder.encodeEnumeration(MarketOperation)
}
