package investapi.investmodels

import investapi.investmodels.MarketOperation.MarketOperation

case class MarketOrder(lots: Long, operation: MarketOperation)
