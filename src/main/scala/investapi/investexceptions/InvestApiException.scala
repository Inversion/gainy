package investapi.investexceptions

class InvestApiException(msg: String) extends Exception(msg) {
}
