package investapi

import scala.concurrent.Future
import com.github.nscala_time.time.Imports._
import investapi.investmodels.{Candles, MarketOrderResponse}
import investapi.investmodels.CandlesInterval.CandlesInterval
import investapi.investmodels.MarketOperation.MarketOperation

trait InvestApi {
  def marketCandles(bearer: String)
                   (figi: String, from: DateTime,
                    to: DateTime, interval: CandlesInterval): Future[Candles]

  def ordersMarketOrder(bearer: String)
                       (figi: String, lots: Long, op: MarketOperation): Future[MarketOrderResponse]
}
