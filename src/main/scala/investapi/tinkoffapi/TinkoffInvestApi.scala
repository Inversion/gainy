package investapi.tinkoffapi

import com.github.nscala_time.time.Imports
import sttp.client3._

import scala.concurrent.{ExecutionContext, Future}
import io.circe.generic.auto._
import investapi.InvestApi
import investapi.investmodels.{Candles, MarketOrder, MarketOrderResponse}
import investapi.investmodels.CandlesInterval.CandlesInterval
import investapi.investmodels.MarketOperation.MarketOperation
import investapi.tinkoffapi.restclient.RestClient

class TinkoffInvestApi(baseUrl: String)
                      (implicit val backend: SttpBackend[Future, Any],
                       implicit val ec: ExecutionContext) extends InvestApi {

  private val client = new RestClient

  override def marketCandles(bearer: String)
                            (figi: String,
                             from: Imports.DateTime, to: Imports.DateTime,
                             interval: CandlesInterval): Future[Candles] =
    client.get[Candles](bearer)(
      uri"$baseUrl/market/candles?figi=$figi&from=$from&to=$to&interval=$interval"
    )

  override def ordersMarketOrder(bearer: String)
                                (figi: String, lots: Long, op: MarketOperation): Future[MarketOrderResponse] =
    client.post[MarketOrderResponse, MarketOrder](bearer)(
      uri"$baseUrl/oderes/market-order?figi=$figi",
      MarketOrder(lots, op)
    )
}
