package investapi.tinkoffapi.restclient

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class ApiError(
                     message: String,
                     code: String
                   )