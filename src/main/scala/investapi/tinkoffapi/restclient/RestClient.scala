package investapi.tinkoffapi.restclient

import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._

import sttp.client3.circe._
import sttp.client3._
import sttp.model.Uri

import scala.concurrent.{ExecutionContext, Future}

import investapi.investexceptions.InvestApiException

class RestClient(implicit val backend: SttpBackend[Future, Any],
                 implicit val ec: ExecutionContext) {
  type RequestType = Request[Either[String, String], Any]
  type ResponseType[T] = Response[Either[ResponseException[ApiResponse[ApiError], io.circe.Error], ApiResponse[T]]]

  def get[T: Decoder](bearer: String)(url: Uri): Future[T] =
    sendRecieve(bearer)(basicRequest.get(url))

  private def sendRecieve[T: Decoder](bearer: String)(request: RequestType): Future[T] =
    request
      .auth.bearer(bearer)
      .response(asJsonEither[ApiResponse[ApiError], ApiResponse[T]])
      .send(backend)
      .flatMap(handleResponse)

  private def handleResponse[T](resp: ResponseType[T]): Future[T] =
    resp.body match {
      case Left(respExcept) => respExcept match {
        case de@DeserializationException(_, _) => Future.failed(de)
        case HttpError(apiResp, _) =>
          Future.failed(new InvestApiException(apiResp.payload.message))
      }
      case Right(apiResp) => Future.successful(apiResp.payload)
    }

  def post[T: Decoder, U: Encoder](bearer: String)(url: Uri, body: U): Future[T] =
    sendRecieve(bearer)(basicRequest.post(url).body(body))
}
