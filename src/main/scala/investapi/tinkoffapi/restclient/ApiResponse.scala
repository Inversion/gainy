package investapi.tinkoffapi.restclient

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class ApiResponse[T](
                           trackingId: String,
                           status: String,
                           payload: T
                         )
