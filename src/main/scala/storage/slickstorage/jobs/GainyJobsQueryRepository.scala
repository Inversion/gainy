package storage.slickstorage.jobs

import models.{GainyJob, GainyJobState}
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery
import storage.slickstorage.users.GainyUsersQueryRepository
import storage.slickstorage.utils.{DIOAlias, TypeMappers}

object GainyJobsQueryRepository extends TypeMappers with DIOAlias {

  val GainyJobs = TableQuery[GainyJobsTable]

  def insert(userId: Long, apiToken: String,
             figi: String, lots: Long, refPrice: BigDecimal, profit: BigDecimal,
             loss: BigDecimal): DIO[GainyJob, Effect.Write] =
    (GainyJobs returning GainyJobs.map(_.id) into ((job, id) => job.copy(id = id))) +=
      GainyJob(0, userId, apiToken, figi, lots, refPrice, profit, loss, GainyJobState.Running)

  def list(login: String): DIO[Seq[GainyJob], Effect.Read] =
    GainyJobs
      .join(GainyUsersQueryRepository.GainyUsers)
      .on(_.userId === _.id)
      .filter { case (_, user) => user.login === login }
      .map { case (job, _) => job }
      .result

  def listRunning(): DIO[Seq[GainyJob], Effect.Read] =
    GainyJobs
      .filter(_.state === GainyJobState.Running)
      .result
}
