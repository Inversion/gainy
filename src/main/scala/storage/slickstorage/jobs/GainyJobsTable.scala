package storage.slickstorage.jobs

import models.GainyJobState.GainyJobState
import models.{GainyJob, GainyUser}
import slick.jdbc.H2Profile.api._
import slick.lifted.{ForeignKeyQuery, ProvenShape}
import storage.slickstorage.users.{GainyUsersQueryRepository, GainyUsersTable}
import storage.slickstorage.utils.TypeMappers

class GainyJobsTable(tag: Tag)
  extends Table[GainyJob](tag, "GAINY_JOBS")
    with TypeMappers {

  def user: ForeignKeyQuery[GainyUsersTable, GainyUser] = foreignKey(
    "USER_FK",
    userId,
    GainyUsersQueryRepository.GainyUsers
  )(_.id)

  override def * : ProvenShape[GainyJob] =
    (id, userId, apiToken, figi, lots, refPrice, profit, loss, state).mapTo[GainyJob]

  def id: Rep[Long] = column("JOB_ID", O.PrimaryKey, O.AutoInc)

  def userId: Rep[Long] = column("USER_ID")

  def apiToken: Rep[String] = column("API_TOKEN")

  def figi: Rep[String] = column("FIGI")

  def lots: Rep[Long] = column("LOTS")

  def refPrice: Rep[BigDecimal] = column("REF_PRICE")

  def profit: Rep[BigDecimal] = column("PROFIT")

  def loss: Rep[BigDecimal] = column("LOSS")

  def state: Rep[GainyJobState] = column("STATE")
}
