package storage.slickstorage

import models.{GainyJob, GainyUser}
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException
import service.exceptions.LoginAlreadyTakenException
import slick.jdbc.H2Profile.api._
import storage.GainyStorage
import storage.slickstorage.jobs.GainyJobsQueryRepository
import storage.slickstorage.users.GainyUsersQueryRepository

import scala.concurrent.{ExecutionContext, Future}

class SlickGainyStorage(db: Database)
                       (implicit ec: ExecutionContext) extends GainyStorage {

  override def insertUser(login: String, hash: String): Future[GainyUser] =
    db.run(GainyUsersQueryRepository.insert(login, hash)).recoverWith {
      case _: JdbcSQLIntegrityConstraintViolationException =>
        Future.failed(new LoginAlreadyTakenException("User with such login already exists"))
    }

  override def findUser(login: String): Future[Option[GainyUser]] =
    db.run(GainyUsersQueryRepository.find(login))

  override def insertJob(userId: Long, apiToken: String,
                         figi: String, lots: Long, refPrice: BigDecimal,
                         profit: BigDecimal, loss: BigDecimal): Future[GainyJob] =
    db.run(GainyJobsQueryRepository.insert(
      userId, apiToken, figi, lots, refPrice, profit, loss
    ))

  override def listJobs(login: String): Future[Seq[GainyJob]] =
    db.run(GainyJobsQueryRepository.list(login))

  override def listRunningJobs(): Future[Seq[GainyJob]] =
    db.run(GainyJobsQueryRepository.listRunning())
}
