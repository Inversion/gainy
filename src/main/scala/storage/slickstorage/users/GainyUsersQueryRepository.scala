package storage.slickstorage.users

import models.GainyUser
import slick.jdbc.H2Profile.api._
import slick.lifted.TableQuery
import storage.slickstorage.utils.DIOAlias

object GainyUsersQueryRepository extends DIOAlias {

  val GainyUsers = TableQuery[GainyUsersTable]

  def insert(login: String, hash: String): DIO[GainyUser, Effect.Write] =
    (GainyUsers
      returning GainyUsers.map(_.id)
      into ((user, id) => user.copy(id = id))) += GainyUser(0, login, hash)

  def find(login: String): DIO[Option[GainyUser], Effect.Read] =
    GainyUsers
      .filter(_.login === login)
      .result
      .headOption
}
