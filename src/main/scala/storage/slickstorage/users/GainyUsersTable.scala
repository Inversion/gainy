package storage.slickstorage.users

import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape

import models.GainyUser

class GainyUsersTable(tag: Tag) extends Table[GainyUser](tag, "GAINY_USERS") {

  override def * : ProvenShape[GainyUser] = (id, login, passwordHash).mapTo[GainyUser]

  def id: Rep[Long] = column("USER_ID", O.PrimaryKey, O.AutoInc)

  def login: Rep[String] = column("USER_LOGIN", O.Unique)

  def passwordHash: Rep[String] = column("USER_PASSWORD_HASH")
}