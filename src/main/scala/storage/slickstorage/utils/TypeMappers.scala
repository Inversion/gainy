package storage.slickstorage.utils

import models.GainyJobState
import models.GainyJobState.GainyJobState
import slick.jdbc.H2Profile.api._

trait TypeMappers {
  implicit val stateMapper = MappedColumnType.base[GainyJobState, String](
    _.toString, GainyJobState.withName(_)
  )
}
