package storage.slickstorage.utils

import slick.dbio.{DBIOAction, Effect, NoStream}

trait DIOAlias {
  type DIO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
}
