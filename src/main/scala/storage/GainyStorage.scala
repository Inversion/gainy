package storage

import models.{GainyJob, GainyUser}

import scala.concurrent.Future

trait GainyStorage {

  def insertUser(login: String, hash: String): Future[GainyUser]

  def findUser(login: String): Future[Option[GainyUser]]

  def insertJob(userId: Long, apiToken: String,
                figi: String, lots: Long, refPrice: BigDecimal,
                profit: BigDecimal, loss: BigDecimal): Future[GainyJob]

  def listJobs(login: String): Future[Seq[GainyJob]]

  def listRunningJobs(): Future[Seq[GainyJob]]
}
