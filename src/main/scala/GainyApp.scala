import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import api.httpapi.GainyHttpApi
import config.GainyAppConfig
import investapi.tinkoffapi.TinkoffInvestApi
import pureconfig.ConfigSource
import service.GainyService
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import storage.slickstorage.SlickGainyStorage
import storage.slickstorage.jobs.GainyJobsQueryRepository.GainyJobs
import storage.slickstorage.users.GainyUsersQueryRepository.GainyUsers
import sttp.client3.akkahttp.AkkaHttpBackend

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import pureconfig.generic.auto._

object GainyApp {
  def main(args: Array[String]): Unit = {
    val config = ConfigSource.default.load[GainyAppConfig].right.get

    val dbUrl = s"jdbc:h2:mem:${UUID.randomUUID()}"
    val db = Database.forURL(
      dbUrl,
      driver = "org.h2.Driver",
      keepAliveConnection = true
    )

    val create = (GainyUsers.schema ++ GainyJobs.schema).create
    Await.result(db.run(create), 10.seconds)

    implicit val system = ActorSystem()
    implicit val ec = scala.concurrent.ExecutionContext.global
    implicit val backend = AkkaHttpBackend()

    val storage = new SlickGainyStorage(db)
    val investApi = new TinkoffInvestApi(config.investApiBaseUrl)
    val service = new GainyService(config.serviceConfig, investApi, storage)
    val api = new GainyHttpApi(service)

    val bind = Http()
      .newServerAt(config.host, config.port)
      .bind(api.routes)

    // TODO How to do it more convenient?
    system.scheduler.scheduleWithFixedDelay(
      config.startUpDelay.seconds, config.triggerDelay.seconds
    )(() => service.triggerJobs())
  }
}
