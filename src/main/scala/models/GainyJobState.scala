package models

import io.circe.{Decoder, Encoder}

object GainyJobState extends Enumeration {
  type GainyJobState = Value
  val Running = Value("Running")
  val Succeed = Value("Succeed")
  val Failed = Value("Failed")

  implicit val decoder: Decoder[GainyJobState] = Decoder.decodeEnumeration(GainyJobState)
  implicit val encoder: Encoder[GainyJobState] = Encoder.encodeEnumeration(GainyJobState)
}
