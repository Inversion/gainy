package models

case class GainyUser(
                      id: Long,
                      login: String,
                      hash: String
                    )
