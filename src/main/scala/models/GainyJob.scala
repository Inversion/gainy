package models

import models.GainyJobState.GainyJobState

case class GainyJob(
                     id: Long,
                     userId: Long,
                     apiToken: String,
                     figi: String,
                     lots: Long,
                     refPrice: BigDecimal,
                     profit: BigDecimal,
                     loss: BigDecimal,
                     state: GainyJobState
                   )
