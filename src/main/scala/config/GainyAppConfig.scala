package config

import service.serviceconfig.GainyConfig

case class GainyAppConfig(
                           host: String,
                           port: Int,
                           investApiBaseUrl: String,
                           startUpDelay: Int,
                           triggerDelay: Int,
                           serviceConfig: GainyConfig
                         )
