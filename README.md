# gainy

StopLoss, TakeProfit сервис для инвестиций ~~интегрированный с
Тинькофф.Инвестиции~~ (не закончено).

## Использованные технологии

- [akka-http](https://github.com/akka/akka-http) - HTTP Backend
- [sttp](https://github.com/softwaremill/sttp) - HTTP Client
- [circe](https://github.com/circe/circe) - JSON serialization/deserialization
- [jwt-circe](https://github.com/pauldijou/jwt-scala) - JWT support
- [akka-http-circe](https://github.com/hseeberger/akka-http-json) - Circe support for akka-http
- [slick](https://github.com/slick/slick) - Database management/connection
- [pureconfig](https://github.com/pureconfig/pureconfig) - Config reading
- [scala-bcrypt](https://github.com/t3hnar/scala-bcrypt) - Password hashing
- [nscala-time](https://github.com/nscala-time/nscala-time) - DateTime management
- [scala-test](https://github.com/scalatest/scalatest) - Testing
- [scala-mock](https://github.com/paulbutcher/ScalaMock) - Mocking for testing

## Работа с сервисом

При запуске сервис создаёт на указанном в `application.conf` адресе
сервер, предоставляющий HTTP Rest API:

- `POST /user/register` - регистрация пользователя
- `GET /user/gettoken` - получение токена для работы с сервисом
- `POST /job/create` - создание задания
- `GET /job/list` - получение списка заданий

Полное описание методов - [OpenAPI Specification](api-spec.yaml).

После регистрации и создания задания пользователем, сервис обрабатывает все
активные задания с интервалом, указанным в `application.conf`, запрашивая
свечи у биржи и выставляя на продажу те позиции, для которых
цена выше `refPrice * (1 + profit)` или ниже, чем `refPrice * (1 - loss)`.

## Структура проекта

Проект раздалён на компоненты:

- `storage` - работа с данными приложения
- `investapi` - работа с запросами к api биржи (Тинькофф.Инвестиции)
- `service` - логика работы сервиса, зависит от `storage` и `investapi`
- `api.httpapi` - предоставление доступа к `service` через HTTP RestAPI

## TODO

- Разработать большую интеграцию с биржей: обрабатывать ответы и менять состояния заданий
- Добавить логирование