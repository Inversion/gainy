name := "gainy"

version := "0.1"

scalaVersion := "2.13.4"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.1"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion
)

libraryDependencies ++= Seq(
  "com.softwaremill.sttp.client3" %% "core" % "3.0.0-RC9",
  "com.softwaremill.sttp.client3" %% "circe" % "3.0.0-RC9",
  "com.softwaremill.sttp.client3" %% "akka-http-backend" % "3.0.0-RC9"
)

val circeVersion = "0.12.3"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.3",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3"
)

libraryDependencies += "com.h2database" % "h2" % "1.4.200"

libraryDependencies += "de.heikoseeberger" %% "akka-http-circe" % "1.31.0"

libraryDependencies ++= Seq(
  "com.pauldijou" %% "jwt-circe" % "4.2.0"
)

libraryDependencies += "com.github.t3hnar" %% "scala-bcrypt" % "4.1"

libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.14.0"

libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.26.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % "test"
libraryDependencies += "org.scalamock" %% "scalamock" % "4.4.0" % Test